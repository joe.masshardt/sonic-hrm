# Sonic HRM

**SonicHRM** is an web application intended for managing heuman resouce and payroll of a small business organization. This project is a guinea pig of my Dot Net Core learning. For that reasone application may not be 100% ready for deployment. Any suggestions, isses will be wolcomed.

## Getting Started

To getting started with this project:
1. Clone the repository
2. Run **Update&Run.bat** file.
    This file will update the repo into latest version, update local database, build the project and run it using DotNet CLI.
    If you want you can enter the commands as your own.
    ```bash
    git pull origin master
    dotnet ef database update --project SonicHRM
    dotnet build
    dotnet run --project SonicHRM
    ```
    *If encountered any issue in creating database using dotnet CLI, consider visiting [this issue.](https://gitlab.com/hard-rox/sonic-hrm/issues/1)*

### Prerequisites

Things have to installed to continue...
1. [DotNet Core SDK 2.2.X or higher](https://dotnet.microsoft.com/download).
2. [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads).
Pretty much it.


## Built With

* [DotNet Core SDK 2.2.X](https://dotnet.microsoft.com/download) - The framework used
* [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads). - Database Engine
* [Visual Studio 2017](https://visualstudio.microsoft.com/downloads/) - IDE
* [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017) - Database Management Studio
* [Git](https://git-scm.com/downloads) - For versioning.

## Authors

* **Rasedur Rahman Roxy** - *Planning and Development* - [Twitter](https://twitter.com/roxyxmw?lang=en)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
