﻿namespace SonicHRM.Helpers
{
    public enum ViewAlertTypes
    {
        Danger,
        Information,
        Warning,
        Success
    }

    public class ViewAlertParam
    {
        public ViewAlertTypes AlertType { get; set; }
        public string Messege { get; set; }
    }
}
