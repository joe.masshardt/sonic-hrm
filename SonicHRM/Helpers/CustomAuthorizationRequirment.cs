﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using SonicHRM.DomainLogics;

namespace SonicHRM.Helpers
{
    public class CustomAuthorizationRequirment : AuthorizationHandler<CustomAuthorizationRequirment>, IAuthorizationRequirement
    {
        private readonly AccountLogics _accountManager;

        public CustomAuthorizationRequirment(AccountLogics accountManager)
        {
            _accountManager = accountManager;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomAuthorizationRequirment requirement)
        {
            //if (!context.User.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
            //{
            //    return Task.CompletedTask;
            //}

            //if (context.User.IsInRole("Developer"))
            //{
            //    context.Succeed(requirement);
            //    return Task.CompletedTask;
            //}

            //var userId = context.User.FindFirstValue(ClaimTypes.NameIdentifier);
            //var user = _accountManager.GetUser(userId);

            //var filterContext = context.Resource as AuthorizationFilterContext;
            //var descriptor = filterContext?.ActionDescriptor as ControllerActionDescriptor;
            //var controller = descriptor?.ControllerName;
            //var action = descriptor?.ActionName;

            //if (user.PermittedMenues.FirstOrDefault(pm =>
            //        pm.Menu.ControllerName == controller && pm.Menu.ActionName == action) != null)
            //{
            //    context.Succeed(requirement);
            //}
            //else
            //{
            //    context.Fail();
            //}
            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
