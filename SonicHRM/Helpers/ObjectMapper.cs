﻿namespace SonicHRM.Helpers
{
    public static class ObjectMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class 
    {
        public static void Map(ref TDestination dest, TSource sourceObject)
        {
            var sourceProperties = sourceObject.GetType().GetProperties();
            foreach (var property in sourceProperties)
            {
                var destProperty = dest.GetType().GetProperty(property.Name);
                if(destProperty != null && destProperty.CanWrite)
                    destProperty.SetValue(dest, property.GetValue(sourceObject), null);
            }
        }
    }
}
