﻿using System;

namespace SonicHRM.Helpers
{
    public static class StaticProperties
    {
        public static DateTime GetCurrentDateTime()
        {
            return DateTime.UtcNow.AddHours(6);
        }
    }
}
