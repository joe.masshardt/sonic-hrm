﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using SonicHRM.Models.ApplicationModels;

namespace SonicHRM.Helpers
{
    public static class FormFileToImageHelper
    {
        public static Image ConvertToImage(IFormFile file)
        {
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                var base64String = Convert.ToBase64String(fileBytes);
                var image = new Image()
                {
                    FileName = file.FileName,
                    ImageDataBase64 = "data:" + file.ContentType + ";base64," + base64String
                };
                return image;
            }
        }
    }
}
