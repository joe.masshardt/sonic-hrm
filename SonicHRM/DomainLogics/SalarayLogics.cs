﻿using System;
using System.Collections.Generic;
using System.Linq;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Models.Enums;
using SonicHRM.Models.ViewModels;
using SonicHRM.Persistence;

namespace SonicHRM.DomainLogics
{
    public class SalaryLogics
    {
        private readonly SalaryDataAccess _salaryDataAccess;
        private readonly AccountDataAccess _accountDataAccess;

        public SalaryLogics(SalaryDataAccess salaryDataAccess, AccountDataAccess accountDataAccess)
        {
            _salaryDataAccess = salaryDataAccess;
            _accountDataAccess = accountDataAccess;
        }

        public List<Salary> GetAllSalaryInfo()
        {
            var salaries = _salaryDataAccess.GetAll().ToList();
            return salaries;
        }

        public bool NewSalaryInfo(Salary salary)
        {
            var result = _salaryDataAccess.Add(salary);
            return result;
        }

        public List<CalculatedSalaryViewModel> CalculateSalary(DateTime fromDate, DateTime toDate, decimal bonus)
        {
            var result = new List<CalculatedSalaryViewModel>();
            var employees = _accountDataAccess.GetAll();
            foreach (var applicationUser in employees)
            {
                //var punches = applicationUser.Punches
                //    .Where(p => p.WorkingDateTime >= fromDate && p.WorkingDateTime <= toDate)
                //    .OrderBy(p => p.WorkingDateTime)
                //    .ToList();
                //if(punches.Count == 0) continue;
                //if (punches[0].Punchtype == Punchtype.OutPunch) punches.Remove(punches[0]);
                ////if (punches[punches.Count - 1].Punchtype == Punchtype.InPunch) punches.Remove(punches[punches.Count - 1]);
                //var workingTime = 0.0;
                //for (var i = 0; i < punches.Count; i++)
                //{
                //    if (punches[i].Punchtype == Punchtype.OutPunch)
                //    {
                //        workingTime += (punches[i].WorkingDateTime - punches[i - 1].WorkingDateTime).TotalHours;
                //    }
                //}

                result.Add(new CalculatedSalaryViewModel()
                {
                    Name = applicationUser.Name,
                    Email = applicationUser.Email,
                    Designation = applicationUser.Designation.DesignationName,
                    TimeFrom = fromDate,
                    TimeTo = toDate,
                    WorkingDays = (toDate - fromDate).Days,
                    BasicSalary = applicationUser.Designation.SalaryInfo.BasicSalary,
                    HouseRent = applicationUser.Designation.SalaryInfo.HouseRentPercent,
                    MedicalAllowence = applicationUser.Designation.SalaryInfo.MedicalAllowancePercent,
                    GrossSalary = applicationUser.Designation.SalaryInfo.BasicSalary +
                                  (applicationUser.Designation.SalaryInfo.BasicSalary *
                                   applicationUser.Designation.SalaryInfo.HouseRentPercent / 100) +
                                  (applicationUser.Designation.SalaryInfo.BasicSalary *
                                   applicationUser.Designation.SalaryInfo.ConveyenceAllowancePercent / 100) +
                                  (applicationUser.Designation.SalaryInfo.BasicSalary * bonus / 100)
                });
            }
            return result;
        }
    }
}
