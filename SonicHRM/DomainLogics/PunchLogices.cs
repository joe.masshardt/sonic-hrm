﻿using System.Collections.Generic;
using System.Linq;
using SonicHRM.Helpers;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Models.Enums;
using SonicHRM.Persistence;

namespace SonicHRM.DomainLogics
{
    public class PunchLogices
    {
        private readonly PunchDataAccess _punchDataAccess;

        public PunchLogices(PunchDataAccess punchDataAccess)
        {
            _punchDataAccess = punchDataAccess;
        }

        public List<Punch> GetTodaysPunches(string userId)
        {
            var result = _punchDataAccess.GetAll()
                .Where(p => p.WorkingDateTime.Date == StaticProperties.GetCurrentDateTime().Date &&
                            p.ApplicationUserId == userId).ToList();
            return result;
        }
    }
}
