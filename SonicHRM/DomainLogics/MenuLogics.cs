﻿using System.Collections.Generic;
using System.Linq;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence;

namespace SonicHRM.DomainLogics
{
    public class MenuLogics
    {
        private readonly MenuDataAccess _menuData;

        public MenuLogics(MenuDataAccess menuData)
        {
            _menuData = menuData;
        }

        public List<Menu> GetAllMenu()
        {
            var menues = _menuData.GetAll().ToList();
            return menues;
        }
    }
}
