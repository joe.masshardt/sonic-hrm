﻿using System.Diagnostics;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SonicHRM.DomainLogics;
using SonicHRM.Models.ViewModels;

namespace SonicHRM.Controllers
{
    [Authorize(Policy = "CustomAuthorization")]
    public class HomeController : Controller
    {
        private readonly AccountLogics _accountManager;

        public HomeController(AccountLogics accountManager)
        {
            _accountManager = accountManager;
        }

        [Authorize]
        public IActionResult Index()
        {
            var user = _accountManager.GetUser(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            return View(user);
        }

        public IActionResult Dashboard()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
