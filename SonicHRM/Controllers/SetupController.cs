﻿using System;
using Microsoft.AspNetCore.Mvc;
using SonicHRM.DomainLogics;
using SonicHRM.Helpers;
using SonicHRM.Models.ApplicationModels;

namespace SonicHRM.Controllers
{
    public class SetupController : Controller
    {
        private readonly AccountLogics _accountLogics;
        private readonly DesignationLogics _designationLogics;
        private readonly SalaryLogics _salarayLogics;

        public SetupController(DesignationLogics designationLogics, SalaryLogics salarayLogics, AccountLogics accountLogics)
        {
            _designationLogics = designationLogics;
            _salarayLogics = salarayLogics;
            _accountLogics = accountLogics;
        }

        public IActionResult SalarySetup()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SalarySetup(Salary salary)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Warning,
                    Messege = "Inappropriate information. Salary Info not saved. Please try again."
                };
                return View();
            }
            try
            {
                if (_salarayLogics.NewSalaryInfo(salary))
                {
                    ViewData["Alert"] = new ViewAlertParam()
                    {
                        AlertType = ViewAlertTypes.Success,
                        Messege = "Salary Saved Successfully."
                    };
                    return View();
                }
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Information,
                    Messege = "Inappropriate information. Salary Info not saved. Please try again."
                };
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Danger,
                    Messege = "Something unusual happed. Salary Info not saved. Please try again."
                };
                return View();
            }
        }

        public IActionResult DesignationSetup()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DesignationSetup(Designation designation)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Warning,
                    Messege = "Inappropriate information. Designation Info not saved. Please try again."
                };
                return View();
            }
            try
            {
                if (_designationLogics.SaveDesignation(designation))
                {
                    ViewData["Alert"] = new ViewAlertParam()
                    {
                        AlertType = ViewAlertTypes.Success,
                        Messege = "Designation Saved Successfully."
                    };
                    return View();
                }
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Information,
                    Messege = "Inappropriate information. Designation Info not saved. Please try again."
                };
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Danger,
                    Messege = "Something unusual happed. Designation Info not saved. Please try again."
                };
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetPermittedMenues(string userId)
        {
            var user = _accountLogics.GetUser(userId);
            return Json(user.PermittedMenues);
        }
        public IActionResult MenuPermission()
        {
            return View();
        }
    }
}