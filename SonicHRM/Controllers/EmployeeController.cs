﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SonicHRM.DomainLogics;
using SonicHRM.Helpers;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Models.ViewModels;

namespace SonicHRM.Controllers
{
    [Authorize(Policy = "CustomAuthorization")]
    public class EmployeeController : Controller
    {
        private readonly AccountLogics _accountLogics;
        private readonly SalaryLogics _salaryLogics;

        public EmployeeController(AccountLogics accountLogics, SalaryLogics salaryLogics)
        {
            _accountLogics = accountLogics;
            _salaryLogics = salaryLogics;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(EmployeeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Danger,
                    Messege = "Inappropriate information. Employee not saved. Please try again."
                };
                return View();
            }
            try
            {
                var newUser = new ApplicationUser
                {
                    UserName = model.RegisteredEmail,
                    Email = model.RegisteredEmail
                };
                ObjectMapper<EmployeeViewModel, ApplicationUser>.Map(ref newUser, model);
                var postedFiles = HttpContext.Request.Form.Files;
                newUser.Photo = FormFileToImageHelper.ConvertToImage(postedFiles[0]);
                var regAcc =
                    _accountLogics.CreateUser(newUser);
                if (regAcc != IdentityResult.Success)
                {
                    ViewData["Alert"] = new ViewAlertParam()
                    {
                        AlertType = ViewAlertTypes.Danger,
                        Messege = "Employee not saved. Please try again."
                    };
                    return View();
                }
                ModelState.Clear();
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Success,
                    Messege = "Employee saved successfully."
                };
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewData["Alert"] = new ViewAlertParam()
                {
                    AlertType = ViewAlertTypes.Danger,
                    Messege = "There is some error. Please try again."
                };
                return View();
            }
        }

        public IActionResult CalculateSalary()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CalculateSalary(CalculateSalaryViewModel model)
        {
            if (model.BonusPercent == null) model.BonusPercent = 0;
            var result = _salaryLogics.CalculateSalary((DateTime)model.StartDate, (DateTime)model.EndDate, (decimal)model.BonusPercent);
            ViewBag.Salaries = result;
            return View();
        }

        public IActionResult Bonus()
        {
            return View();
        }
    }
}