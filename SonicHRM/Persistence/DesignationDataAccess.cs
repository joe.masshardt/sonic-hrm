﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class DesignationDataAccess : GenericDataAccess<Designation>
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public DesignationDataAccess(ApplicationDbContext db) : base(db)
        {
            _applicationDbContext = db;
        }
    }
}
