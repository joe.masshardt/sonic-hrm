﻿using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class SalaryDataAccess : GenericDataAccess<Salary>
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public SalaryDataAccess(ApplicationDbContext db) : base(db)
        {
            _applicationDbContext = db;
        }
    }
}
