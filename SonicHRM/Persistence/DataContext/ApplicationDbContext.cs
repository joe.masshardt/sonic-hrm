﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SonicHRM.Models.ApplicationModels;

namespace SonicHRM.Persistence.DataContext
{
    public class ApplicationDbContext:IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        public DbSet<Designation> Designations { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<LeaveApplication> LeaveApplications { get; set; }
        public DbSet<Punch> Punches { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<PermittedMenu> PermittedMenus { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Configuring Many To Many between ApplicationUser and Menues
            builder.Entity<PermittedMenu>()
                .HasOne(pc => pc.ApplicationUser)
                .WithMany(pc => pc.PermittedMenues)
                .HasForeignKey(pc => pc.ApplicationUserId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<PermittedMenu>()
                .HasOne(pc => pc.Menu)
                .WithMany(pc => pc.MenuPermissions)
                .HasForeignKey(pc => pc.MenuId)
                .OnDelete(DeleteBehavior.Cascade);

            #region IdentityRoles Seed Data

            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Name = "Developer" }
            );

            #endregion

            #region IdentityUser Seed Data

            //builder.Entity<IdentityUser>().HasData(
            //    new IdentityUser()
            //    {
            //        Id = "716bd48e-49cc-411f-80f1-8a9ad4eb4159",
            //        Email = "dev@sonic.hrm",
            //        UserName = "dev@sonic.hrm",
            //        PasswordHash = "AQAAAAEAACcQAAAAEFOvHA3YORSK2S4shT+3Zc2aU/7llKAVIgpgWQQPeA2WAnhm6FpNEFIM1iU/EfjD1g==",

            //    }
            //);

            #endregion

            #region Menue Seed Data

            builder.Entity<Menu>().HasData(
                new Menu() { Id = 1, MenueName = "Dashboard", ActionName = "Dashboard", ControllerName = "Home", IconClass = "fa fa-tachometer" },
                new Menu() { Id = 2, MenueName = "Profile", ActionName = "Index", ControllerName = "Home", IconClass = "fa fa-user" },
                new Menu() { Id = 3, MenueName = "Setup", ActionName = "", ControllerName = "", IconClass = "fa fa-cogs" },
                new Menu() { Id = 7, MenueName = "Salary Setup", ActionName = "SalarySetup", ControllerName = "Setup", IconClass = "fa fa-money", ParentMenuId = 3 },
                new Menu() { Id = 4, MenueName = "Designation Setup", ActionName = "DesignationSetup", ControllerName = "Setup", IconClass = "fa fa-black-tie", ParentMenuId = 3},
                new Menu() { Id = 5, MenueName = "Employee", ActionName = "", ControllerName = "", IconClass = "fa fa-users" },
                new Menu() { Id = 6, MenueName = "Employee Management", ActionName = "Index", ControllerName = "Employee", IconClass = "fa fa-user", ParentMenuId = 5},
                new Menu() { Id = 8, MenueName = "Calculate Salary", ActionName = "CalculateSalary", ControllerName = "Employee", IconClass = "fa fa-calculator", ParentMenuId = 5},
                new Menu() { Id = 9, MenueName = "Bonus", ActionName = "Bonus", ControllerName = "Employee", IconClass = "fa fa-calculator", ParentMenuId = 5}
                //new Menu() { Id = 10, MenueName = "Menu Permission", ActionName = "MenuPermission", ControllerName = "Setup", IconClass = "fa fa-calculator", ParentMenuId = 3}
            );

            #endregion
        }
    }
}
