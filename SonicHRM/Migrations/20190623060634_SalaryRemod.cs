﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class SalaryRemod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "95daf167-3051-4a1b-94e9-8c4a379fb183", "07e6d014-6a43-4bb8-9e88-620a4224ccd3" });

            migrationBuilder.AddColumn<decimal>(
                name: "ConveyenceAllowancePercent",
                table: "Salaries",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d886008b-c591-498c-b921-9517f2856181", "a71270ca-c987-4f51-9369-bc351d05de3f", "Developer", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "d886008b-c591-498c-b921-9517f2856181", "a71270ca-c987-4f51-9369-bc351d05de3f" });

            migrationBuilder.DropColumn(
                name: "ConveyenceAllowancePercent",
                table: "Salaries");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "95daf167-3051-4a1b-94e9-8c4a379fb183", "07e6d014-6a43-4bb8-9e88-620a4224ccd3", "Developer", null });
        }
    }
}
