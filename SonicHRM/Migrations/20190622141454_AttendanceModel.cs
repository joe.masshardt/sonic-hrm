﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class AttendanceModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Image_ProfileImageId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ProfileImageId",
                table: "AspNetUsers");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "3c27230e-78b1-496e-8b63-0d036717c5d8", "dcea6c36-e337-4b57-ac25-f5f572128299" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "53c39ea1-4881-44b0-9bad-23c32c377522", "8b7ca4b3-59cb-48f4-8d11-b92d80b6423e" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "55deb667-60fa-4e2b-ba02-3d982c47cdba", "553b266d-3b89-4280-bf80-a877b65ed82b" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "6f766646-986c-4635-9c8a-82e033fa3a82", "7b923f79-06ec-49a1-8352-0f04ec2e1df0" });

            migrationBuilder.DropColumn(
                name: "ProfileImageId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<decimal>(
                name: "Salary",
                table: "Employees",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "ab993fc7-dd5d-4fe8-bbd6-4624b77ef6ec", "00e82590-3a57-4bfb-aecd-aefc1e2c9187", "Developer", null },
                    { "2149d256-54b2-4d2e-a77b-0bb95cf50187", "36b04551-5f98-4bc9-8e6d-bdac22b3cc13", "Employee", null }
                });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                column: "ParentMenuId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                column: "ParentMenuId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                column: "ParentMenuId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                column: "ParentMenuId",
                value: 5);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "2149d256-54b2-4d2e-a77b-0bb95cf50187", "36b04551-5f98-4bc9-8e6d-bdac22b3cc13" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "ab993fc7-dd5d-4fe8-bbd6-4624b77ef6ec", "00e82590-3a57-4bfb-aecd-aefc1e2c9187" });

            migrationBuilder.DropColumn(
                name: "Salary",
                table: "Employees");

            migrationBuilder.AddColumn<int>(
                name: "ProfileImageId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "53c39ea1-4881-44b0-9bad-23c32c377522", "8b7ca4b3-59cb-48f4-8d11-b92d80b6423e", "Developer", null },
                    { "3c27230e-78b1-496e-8b63-0d036717c5d8", "dcea6c36-e337-4b57-ac25-f5f572128299", "Admin", null },
                    { "55deb667-60fa-4e2b-ba02-3d982c47cdba", "553b266d-3b89-4280-bf80-a877b65ed82b", "HR Manager", null },
                    { "6f766646-986c-4635-9c8a-82e033fa3a82", "7b923f79-06ec-49a1-8352-0f04ec2e1df0", "Employee", null }
                });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                column: "ParentMenuId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                column: "ParentMenuId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                column: "ParentMenuId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                column: "ParentMenuId",
                value: 4);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ProfileImageId",
                table: "AspNetUsers",
                column: "ProfileImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Image_ProfileImageId",
                table: "AspNetUsers",
                column: "ProfileImageId",
                principalTable: "Image",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
