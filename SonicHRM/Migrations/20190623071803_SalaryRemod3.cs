﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class SalaryRemod3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "a46027b0-fc99-4917-be98-afc3197c96c3", "893f7720-e31b-4888-b1bb-8e0e1f8a5d33" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "204d1137-203f-4eb5-9f28-86811bc52643", "24fadc59-c7b0-47e5-b49c-83dd56378606", "Developer", null });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[] { 9, "SalarySetup", "Setup", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-money", false, "Salary Setup", 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "204d1137-203f-4eb5-9f28-86811bc52643", "24fadc59-c7b0-47e5-b49c-83dd56378606" });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a46027b0-fc99-4917-be98-afc3197c96c3", "893f7720-e31b-4888-b1bb-8e0e1f8a5d33", "Developer", null });
        }
    }
}
