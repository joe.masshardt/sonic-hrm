﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class MenuSeed3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "62eefddb-ebb0-4729-a31d-366cee2658ff", "665b0e04-b3f7-4484-8100-33b27451b659" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "fa0c4e0e-ba1f-420b-a190-a1ae7d6e90a7", "c8a6d0bd-b8e4-47ec-8212-e32038e39a55", "Developer", null });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[] { 8, "CalculateSalary", "Employee", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-calculator", false, "Calculate Salary", 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "fa0c4e0e-ba1f-420b-a190-a1ae7d6e90a7", "c8a6d0bd-b8e4-47ec-8212-e32038e39a55" });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "62eefddb-ebb0-4729-a31d-366cee2658ff", "665b0e04-b3f7-4484-8100-33b27451b659", "Developer", null });
        }
    }
}
