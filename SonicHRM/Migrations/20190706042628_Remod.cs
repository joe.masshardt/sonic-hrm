﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class Remod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "fa0c4e0e-ba1f-420b-a190-a1ae7d6e90a7", "c8a6d0bd-b8e4-47ec-8212-e32038e39a55" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "2cde049c-e3ee-48dd-948e-058838e81f6f", "9e4c8da5-1077-457a-a896-711ff041c0c8", "Developer", null });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[] { 9, "Bonus", "Employee", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-calculator", false, "Bonus", 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[] { 10, "MenuPermission", "Setup", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-calculator", false, "Menu Permission", 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "2cde049c-e3ee-48dd-948e-058838e81f6f", "9e4c8da5-1077-457a-a896-711ff041c0c8" });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "fa0c4e0e-ba1f-420b-a190-a1ae7d6e90a7", "c8a6d0bd-b8e4-47ec-8212-e32038e39a55", "Developer", null });
        }
    }
}
