﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SonicHRM.Models.ApplicationModels
{
    public class Designation
    {
        public int Id { get; set; }
        [DisplayName("Designation")]
        public string DesignationName { get; set; }

        public int SalaryInfoId { get; set; }
        public virtual Salary SalaryInfo { get; set; }

        public virtual ICollection<ApplicationUser> Employees { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
