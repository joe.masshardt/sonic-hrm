﻿using System;

namespace SonicHRM.Models.ApplicationModels
{
    public class Salary
    {
        public int Id { get; set; }
        public string SalaryTitle { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal HouseRentPercent { get; set; }
        public decimal MedicalAllowancePercent { get; set; }
        public decimal ConveyenceAllowancePercent { get; set; }
        public decimal TravelAllowance { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
