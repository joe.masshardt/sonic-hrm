﻿using System;
using SonicHRM.Models.Enums;

namespace SonicHRM.Models.ApplicationModels
{
    public class Punch
    {
        public int Id { get; set; }
        public DateTime WorkingDateTime { get; set; }

        public Punchtype Punchtype { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
