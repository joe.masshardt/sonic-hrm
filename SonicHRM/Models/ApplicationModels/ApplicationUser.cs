﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace SonicHRM.Models.ApplicationModels
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Education { get; set; }
        public string NationalId { get; set; }
        public DateTime JoiningDate { get; set; }

        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }

        public int? PhotoId { get; set; }
        public virtual Image Photo { get; set; }

        public virtual ICollection<PermittedMenu> PermittedMenues { get; set; }
        public virtual ICollection<Punch> Punches { get; set; }
        public virtual ICollection<LeaveApplication> LeaveApplications { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
