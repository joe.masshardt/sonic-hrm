﻿using System;

namespace SonicHRM.Models.ApplicationModels
{
    public class LeaveApplication
    {
        public int Id { get; set; }
        public DateTime LeaveFrom { get; set; }
        public DateTime LeaveTo { get; set; }
        public string LeaveType { get; set; }
        public string LeaveReason { get; set; }
        public bool IsApproved { get; set; }
        public string Remarks { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
