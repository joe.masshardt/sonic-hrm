﻿using System;

namespace SonicHRM.Models.ViewModels
{
    public class CalculateSalaryViewModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? BonusPercent { get; set; }
    }
}
