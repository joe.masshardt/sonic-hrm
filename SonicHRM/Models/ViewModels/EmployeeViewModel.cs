﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using SonicHRM.Models.ApplicationModels;

namespace SonicHRM.Models.ViewModels
{
    public class EmployeeViewModel
    {
        [Required]
        [DisplayName("Registered Email")]
        [DataType(DataType.EmailAddress)]
        public string RegisteredEmail { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Education { get; set; }
        [DisplayName("NID No")]
        [Required]
        public string NationalId { get; set; }
        [DisplayName("Joining Date")]
        [Required]
        public DateTime JoiningDate { get; set; }
        [DisplayName("Designation")]
        [Required]
        public int DesignationId { get; set; }
        public string Photo { get; set; }
    }
}
