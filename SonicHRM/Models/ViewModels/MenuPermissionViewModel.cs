﻿namespace SonicHRM.Models.ViewModels
{
    public class MenuPermissionViewModel
    {
        public string UserId { get; set; }
        public int MenuId { get; set; }
    }
}
