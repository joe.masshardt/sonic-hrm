﻿using System;

namespace SonicHRM.Models.ViewModels
{
    public class CalculatedSalaryViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }
        public int WorkingDays { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal HouseRent { get; set; }
        public decimal MedicalAllowence { get; set; }
        public decimal GrossSalary { get; set; }
    }
}
