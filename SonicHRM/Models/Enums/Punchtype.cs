﻿namespace SonicHRM.Models.Enums
{
    public enum Punchtype
    {
        InPunch,
        OutPunch
    }
}